#!/usr/bin/python

#######################################################################################################################
#A python script to easily apply uniaxial stress (along the x-axis) to graphene and run a DFT simulation using FHI-aims
#After running the script, a bandstructure+DOS plot called "aimsplot.png" should be created.
#The bandgap should be printed to the file "bandgap.dat"
#This script was created for the course - simulations and new materials.
#To use the script for strain of magnitude, say, 5%, run the following:
#python3 uniaxial_strain_graphene.py -s 5
#Send feedback to ransell.dsouza@utu.fi/ ransell.d@gmail.com
#######################################################################################################################

#Import needed modules 
import os
import shutil
import numpy as np
from optparse import OptionParser

#strain value would go here
parser = OptionParser()
parser.add_option("-s", help="percentage of strain", action="store",
                    dest="strain", type=float)
(options,args) = parser.parse_args()

if options.strain == None:
	print("You forgot to add strain, The calculation won't have any strain")
	s = 0
else:
	s = options.strain


#stress formula
a0 = 2.46
a = (s/100)*a0 + a0

# Create directory
dirname = "s_"+str(s)
if not os.path.exists(dirname):
  os.makedirs(dirname)
# Change to directory        
os.chdir(dirname)

# Copy the needed files (control and aimsplot)
shutil.copyfile("../control.in","control.in")

no_strain_geometry = """
lattice_vector           %s           0.0000000          0.00000000
lattice_vector           1.23           2.130422493309719  0.00000000
lattice_vector           0.00000000     0.00000000         50.00000000
atom_frac 0.0000000 0.0000000 0.000000 C
atom_frac 0.3333333 0.3333333 0.000000 C
""" % (a)
with open("geometry.in", 'w') as file_tmp:
        file_tmp.write(no_strain_geometry)
        file_tmp.write("\n")

# Run FHI-aims on 4 processes
os.system("srun -n 4 aims.x > output")
os.system("python3 $COURSE/bin/aimsplot.py")
os.system("grep ESTIMATED output | tail -1 | awk '{print $5}' > bandgap.dat")

print("Bandstructure and dos calculations finished with %s strain" % s)

# Strain graphene

## Description

A python script to easily apply uniaxial stress (along the x-axis) to graphene and run a DFT simulation using FHI-aims.
After running the script, a bandstructure+DOS plot called "aimsplot.png" should be created.
The bandgap should be printed to the file "bandgap.dat"

This script was created for the course - "simulations and new materials (MTEK0024)".

## Use
To use the script for strain of magnitude, say, 5%, run the following:

```python
python3 uniaxial_strain_graphene.py -s 5
```
## Learning objectives
1. Bandgap manipulation through strain engineering
2. Calculation of Graphene's Young's modulus

## Feedback
Send feedback to ransell.dsouza@utu.fi/ ransell.d@gmail.com
